// (c) 2016 - 2018: Marcel Admiraal

#ifndef NUGUILISTENER_H
#define NUGUILISTENER_H

#include "nulistener.h"

#include "nuguiframe.h"

namespace nu
{
    class GUIListener : public Listener
    {
    public:
        /**
         * Constructor.
         *
         * @param parent    The frame's parent.
         */
        GUIListener(GUIFrame* frame);

        /**
         * Called when message data received from the IMU.
         *
         * @param data  The message data received.
         */
        virtual void onMessageDataReceived(const ma::Data& data);

        /**
         * Called when accelerometer data received from the IMU.
         *
         * @param data  The accelerometer data received.
         */
        virtual void onAccelerometerDataReceived(const ma::Data& data);

        /**
         * Called when gyroscope data received from the IMU.
         *
         * @param data  The gyroscope data received.
         */
        virtual void onGyroscopeDataReceived(const ma::Data& data);

        /**
         * Called when magnetometer data received from the IMU.
         *
         * @param data  The magnetometer data received.
         */
        virtual void onMagnetometerDataReceived(const ma::Data& data);

        /**
         * Called when quaternion data received from the IMU.
         *
         * @param data  The quaternion data received.
         */
        virtual void onQuaternionReceived(const ma::Data& data);

        /**
         * Called when analogue data received from the IMU.
         *
         * @param data  The analogue data received.
         */
        virtual void onAnalogueDataReceived(const ma::Data& data);

        /**
         * Called when register data received.
         *
         * @param data  Pointer to the register data received.
         */
        virtual void onRegisterDataReceived(const ma::Data& data);

    private:
        GUIFrame* frame;
    };
}

#endif // NUGUILISTENER_H
