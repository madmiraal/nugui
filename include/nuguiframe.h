// (c) 2016 - 2018: Marcel Admiraal

#ifndef NUGUIFRAME_H
#define NUGUIFRAME_H

#include <wx/frame.h>
#include <wx/string.h>
#include <wx/stattext.h>

#include "str.h"
#include "vector.h"
#include "oscilloscopepanel.h"
#include "cuboidframe.h"
#include "vector3dframe.h"
#include "nudata.h"
#include "nuapi.h"

#include <fstream>
#include <chrono>

#define AXES 3

namespace nu
{
    class GUIListener;

    class GUIFrame : public wxFrame
    {
    public:
        /**
         * Constructor.
         *
         * @param parent    The frame's parent.
         */
        GUIFrame(wxWindow* parent);

        /**
         * Default destructor.
         */
        virtual ~GUIFrame();

        /**
         * Sets the status message.
         *
         * @param message   The new status message.
         */
        void setStatusMessage(const ma::Str& message);

        /**
         * Updates the accelerometer data.
         *
         * @param data  The new accelerometer data.
         */
        void accelerometerData(const ma::Vector& data);

        /**
         * Updates the accelerometer frequency data.
         *
         * @param data  The new accelerometer frequency data.
         */
        void accelerometerFrequency(unsigned int frequency);

        /**
         * Updates the gyroscope data.
         *
         * @param data  The new gyroscope data.
         */
        void gyroscopeData(const ma::Vector& data);

        /**
         * Updates the gyroscope frequency data.
         *
         * @param data  The new gyroscope frequency data.
         */
        void gyroscopeFrequency(unsigned int frequency);

        /**
         * Updates the magnetometer data.
         *
         * @param data  The new magnetometer data.
         */
        void magnetometerData(const ma::Vector& data);

        /**
         * Updates the magnetometer frequency data.
         *
         * @param data  The new magnetometer frequency data.
         */
        void magnetometerFrequency(unsigned int frequency);

        /**
         * Updates the quaternion data.
         *
         * @param data  The new quaternion data.
         */
        void quaternionData(const ma::Vector& data);

        /**
         * Updates the quaternion frequency data.
         *
         * @param data  The new quaternion frequency data.
         */
        void quaternionFrequency(unsigned int frequency);

        /**
         * Updates the analogue data.
         *
         * @param data  The new analogue data.
         */
        void analogueData(const ma::Vector& data);

        /**
         * Updates the analogue frequency data.
         *
         * @param data  The new analogue frequency data.
         */
        void analogueFrequency(unsigned int frequency);

        /**
         * Updates the register data.
         *
         * @param data  The new register data.
         */
        void registerDataReceived(const RegisterData& data);

    private:
        void onIdle(wxIdleEvent& evt);
        unsigned int getSampleFrequency();
        bool getRegisterData();
        void getSensorData(ma::Str filename, ma::Vector sensorData[][AXES],
                unsigned int sensorLines[]);
        void filterData(ma::Vector sensorData[][AXES],unsigned int cutoff);
        void plotData(const ma::Vector sensorData[][AXES],
                const unsigned int sensorLines[]);

        void setAPIType(wxCommandEvent& event);
        void setAPIType(unsigned int typeIndex);
        void connect(wxCommandEvent& event);
        bool connect(const ma::Str& port);
        void testDevice(wxCommandEvent& event);
        void sendIMUData(wxCommandEvent& event);
        void sendQuaternions(wxCommandEvent& event);
        void stopSending(wxCommandEvent& event);
        void calibrate(wxCommandEvent& event);
        void command(wxCommandEvent& event);
        void setRegister(wxCommandEvent& event);
        void configureRegisters(wxCommandEvent& event);
        void saveRegistersToFlash(wxCommandEvent& event);
        void freeze(wxCommandEvent& event);
        void display(wxCommandEvent& event);
        void analogueBitsChanged(wxCommandEvent& event);
        void setAnalogueBits(unsigned int analogueBits);
        void childClosed(wxCloseEvent& event);

        API* api;
        GUIListener* listener;
        unsigned int currentAPIType;
        RegisterData registerData;
        volatile bool waitingForRegisterData;

        wxStatusBar* statusBar;
        ma::OscilloscopePanel* accelerometerPanel;
        ma::OscilloscopePanel* gyroscopePanel;
        ma::OscilloscopePanel* magnetometerPanel;
        ma::OscilloscopePanel* quaternionPanel;
        ma::OscilloscopePanel* analoguePanel;
        wxStaticText* accelerometerText;
        wxStaticText* gyroscopeText;
        wxStaticText* magnetometerText;
        wxStaticText* quaternionText;
        wxStaticText* analogueText;
        wxStaticText* analogueBitsText;
        ma::CuboidFrame* imuCuboidFrame;
        ma::Vector3DFrame* accelerometerVectorFrame;
        ma::Vector3DFrame* gyroscopeVectorFrame;
        ma::Vector3DFrame* magnetometerVectorFrame;
        unsigned int accelerometerPackets;
        unsigned int gyroscopePackets;
        unsigned int magnetometerPackets;
        unsigned int analoguePackets;
        unsigned int quaternionPackets;
        std::chrono::nanoseconds lastUpdate;

        bool saveData;
        std::fstream dataFile;

        static const ma::Str sensorName[];
        static const ma::Str axisName[];
        static const unsigned int imuTypes;
        static const wxString imuTypeNames[];
    };
}

#endif // NUGUIFRAME_H
