// (c) 2016 - 2017: Marcel Admiraal

#ifndef NUGUIAPP_H
#define NUGUIAPP_H

#include <wx/app.h>

namespace nu
{
    class GUIApp : public wxApp
    {
    public:
        /**
         * Initialises the application.
         *
         * @return True to continue processing.
         */
        virtual bool OnInit();
    };
}

#endif // NUGUIAPP_H
