// (c) 2016 - 2018: Marcel Admiraal

#include "nuguiframe.h"
#include "nuguilistener.h"

#include <wx/menu.h>
#include <wx/sizer.h>
#include <wx/choicdlg.h>
#include <wx/msgdlg.h>
#include <wx/textdlg.h>
#include <wx/slider.h>

#include "nuconfig.h"
#include "nufiledataapi.h"
#include "nufilerawapi.h"

#include "plot2dpanel.h"
//#include "lowpassfilter.h"

#include <cmath>

//#define portstring "00049_CalInertialAndMag.csv"
//#define initialAPIType 1
//#define portstring "boxingdata.csv"
#define initialAPIType 2
#define portstring "G1T1.csv"
//#define portstring "/dev/rfcomm0"
//#define portstring "/dev/rfcomm1"
//#define portstring "COM17" // IMU030
//#define portstring "COM6"  // IMU050

#define SENSORS 3

enum
{
    ID_TYPE,
    ID_TEST,
    ID_SENDIMUDATA,
    ID_SENDQUATERNIONS,
    ID_STOPSENDING,
    ID_CALIBRATE,
    ID_SET_REGISTER,
    ID_SAVE_TO_FLASH,
    ID_FREEZE,
    ID_DISPLAY_CUBE,
    ID_DISPLAY_ACCELEROMETER,
    ID_DISPLAY_GYROSCOPE,
    ID_DISPLAY_MAGNETOMETER
};

namespace nu
{
    GUIFrame::GUIFrame(wxWindow* parent) :
        wxFrame(parent, wxID_ANY, wxT("nuIMU GUI"), wxDefaultPosition,
        wxSize(386, 334)), api(0), listener(new GUIListener(this)),
        currentAPIType(-1), imuCuboidFrame(0),
        accelerometerVectorFrame(0), gyroscopeVectorFrame(0),
        magnetometerVectorFrame(0),
        accelerometerPackets(0), gyroscopePackets(0),
        magnetometerPackets(0), analoguePackets(0), quaternionPackets(0),
        lastUpdate(
                std::chrono::duration_cast<std::chrono::nanoseconds>(
                std::chrono::steady_clock::now().time_since_epoch())),
        saveData(false)
    {
        SetIcon(wxIcon(wxT("imu.ico")));

        // Create menu.
        wxMenuBar* menuBar = new wxMenuBar();
        wxMenu* controlMenu = new wxMenu();
        controlMenu->Append(ID_TYPE, wxT("API Type"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::setAPIType, this,
                ID_TYPE);
        controlMenu->Append(wxID_FILE, wxT("Connect port"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::connect, this,
                wxID_FILE);
        controlMenu->Append(ID_TEST, wxT("Test accelerometer and gyroscope"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::testDevice, this,
                ID_TEST);
        controlMenu->Append(ID_SENDIMUDATA, wxT("Start sending IMU data"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::sendIMUData, this,
                ID_SENDIMUDATA);
        controlMenu->Append(ID_SENDQUATERNIONS, wxT("Start sending quaternions"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::sendQuaternions, this,
                ID_SENDQUATERNIONS);
        controlMenu->Append(ID_STOPSENDING, wxT("Stop sending data"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::stopSending, this,
                ID_STOPSENDING);
        controlMenu->Append(ID_CALIBRATE, wxT("Calibrate"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::calibrate, this,
                ID_CALIBRATE);
        controlMenu->Append(wxID_EDIT, wxT("Send command"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::command, this,
                wxID_EDIT);
        controlMenu->Append(ID_SET_REGISTER, wxT("Set Register"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::setRegister, this,
                ID_SET_REGISTER);
        controlMenu->Append(wxID_EXECUTE, wxT("Configure registers"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::configureRegisters, this,
                wxID_EXECUTE);
        controlMenu->Append(ID_SAVE_TO_FLASH, wxT("Save registers to flash"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::saveRegistersToFlash, this,
                ID_SAVE_TO_FLASH);
        controlMenu->AppendCheckItem(ID_FREEZE, wxT("Freeze oscilloscopes"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::freeze, this,
                ID_FREEZE);
        menuBar->Append(controlMenu, wxT("Control"));

        wxMenu* displayMenu = new wxMenu();
        displayMenu->Append(ID_DISPLAY_CUBE,
                wxT("Display Cube"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::display, this,
                ID_DISPLAY_CUBE);
        displayMenu->Append(ID_DISPLAY_ACCELEROMETER,
                wxT("Display Accelerometer"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::display, this,
                ID_DISPLAY_ACCELEROMETER);
        displayMenu->Append(ID_DISPLAY_GYROSCOPE,
                wxT("Display Gyroscope"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::display, this,
                ID_DISPLAY_GYROSCOPE);
        displayMenu->Append(ID_DISPLAY_MAGNETOMETER,
                wxT("Display Magnetometer"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::display, this,
                ID_DISPLAY_MAGNETOMETER);
        menuBar->Append(displayMenu, wxT("Display"));

        SetMenuBar(menuBar);

        statusBar = CreateStatusBar();

        wxSizer* frameSizer = new wxBoxSizer(wxVERTICAL);
        accelerometerText = new wxStaticText(this, wxID_ANY,
                wxT("Accelerometer"));
        frameSizer->Add(accelerometerText);
        accelerometerPanel = new ma::OscilloscopePanel(this, 3);
        frameSizer->Add(accelerometerPanel, 1, wxEXPAND);
        gyroscopeText = new wxStaticText(this, wxID_ANY,
                wxT("Gyroscope"));
        frameSizer->Add(gyroscopeText);
        gyroscopePanel = new ma::OscilloscopePanel(this, 3);
        frameSizer->Add(gyroscopePanel, 1, wxEXPAND);
        magnetometerText = new wxStaticText(this, wxID_ANY,
                wxT("Magnetometer"));
        frameSizer->Add(magnetometerText);
        magnetometerPanel = new ma::OscilloscopePanel(this, 3);
        frameSizer->Add(magnetometerPanel, 1, wxEXPAND);
        quaternionText = new wxStaticText(this, wxID_ANY,
                wxT("Quaternion"));
        frameSizer->Add(quaternionText);
        quaternionPanel = new ma::OscilloscopePanel(this, 4);
        frameSizer->Add(quaternionPanel, 1, wxEXPAND);
        wxSizer* analogueSizer = new wxBoxSizer(wxHORIZONTAL);
        analogueText = new wxStaticText(this, wxID_ANY,
                wxT("Analogue"));
        analogueSizer->Add(analogueText, 1, wxEXPAND);
        wxString scaleString = wxT("Bits: (");
        scaleString << DEFAULT_ANALOG_BITS << ")";
        analogueBitsText = new wxStaticText(this, wxID_ANY, scaleString,
                wxDefaultPosition, wxSize(60,1));
        analogueSizer->Add(analogueBitsText, 0, wxEXPAND);
        wxSlider* analogueBitsSlider = new wxSlider(
                this, wxID_ANY, DEFAULT_ANALOG_BITS, 0, 15,
                wxDefaultPosition, wxSize(50, 1));
        analogueBitsSlider->Bind(wxEVT_SLIDER, &GUIFrame::analogueBitsChanged,
                this);
        analogueSizer->Add(analogueBitsSlider, 1, wxEXPAND);
        frameSizer->Add(analogueSizer, 0, wxEXPAND);
        analoguePanel = new ma::OscilloscopePanel(this, 8);
        frameSizer->Add(analoguePanel, 1, wxEXPAND);
        SetSizer(frameSizer);

        Bind(wxEVT_IDLE, &GUIFrame::onIdle, this);

        setAPIType(initialAPIType);
    }

    GUIFrame::~GUIFrame()
    {
        delete api;
        delete listener;
    }

    void GUIFrame::setStatusMessage(const ma::Str& message)
    {
        wxString statusMessage(message.c_str());
        statusBar->GetEventHandler()->CallAfter(
                &wxStatusBar::SetStatusText, statusMessage, 0);
    }

    void GUIFrame::accelerometerData(const ma::Vector& data)
    {
        ++accelerometerPackets;
        ma::Vector fitData = data / api->getAccelerometerScale();
        accelerometerPanel->addData(fitData);
        if (accelerometerVectorFrame != 0)
        {
            ma::Vector3D newVector(data[0], data[1], data[2]);
            accelerometerVectorFrame->setVector(newVector);
        }
        if (saveData)
        {
            dataFile << sensorName[1];
            for (unsigned int i = 0; i < 3; ++i)
                dataFile << "," << data[i];
            dataFile << std::endl;
        }
    }

    void GUIFrame::accelerometerFrequency(unsigned int frequency)
    {
        wxString text = "Accelerometer: ";
        text << frequency << " Hz";
        accelerometerText->GetEventHandler()->CallAfter(
                &wxStaticText::SetLabel, text);
    }

    void GUIFrame::gyroscopeData(const ma::Vector& data)
    {
        ++gyroscopePackets;
        ma::Vector fitData = data / api->getGyroscopeScale();
        gyroscopePanel->addData(fitData);
        if (gyroscopeVectorFrame != 0)
        {
            ma::Vector3D newVector(data[0], data[1], data[2]);
            gyroscopeVectorFrame->setVector(newVector);
        }
        if (saveData)
        {
            dataFile << sensorName[0];
            for (unsigned int i = 0; i < 3; ++i)
                dataFile << "," << data[i];
            dataFile << std::endl;
        }
    }

    void GUIFrame::gyroscopeFrequency(unsigned int frequency)
    {
        wxString text = "Gyroscope: ";
        text << frequency << " Hz";
        gyroscopeText->GetEventHandler()->CallAfter(
                &wxStaticText::SetLabel, text);
    }

    void GUIFrame::magnetometerData(const ma::Vector& data)
    {
        ++magnetometerPackets;
        ma::Vector fitData = data / api->getMagnetometerScale();
        magnetometerPanel->addData(fitData);
        if (magnetometerVectorFrame != 0)
        {
            ma::Vector3D newVector(data[0], data[1], data[2]);
            magnetometerVectorFrame->setVector(newVector);
        }
        if (saveData)
        {
            dataFile << sensorName[2];
            for (unsigned int i = 0; i < 3; ++i)
                dataFile << "," << data[i];
            dataFile << std::endl;
        }
    }

    void GUIFrame::magnetometerFrequency(unsigned int frequency)
    {
        wxString text = "Magnetometer: ";
        text << frequency << " Hz";
        magnetometerText->GetEventHandler()->CallAfter(
                &wxStaticText::SetLabel, text);
    }

    void GUIFrame::quaternionData(const ma::Vector& data)
    {
        ++quaternionPackets;
        //ma::Vector fitData = normalise(data);
        quaternionPanel->addData(data);
        if (imuCuboidFrame != 0)
        {
            ma::Quaternion quaternion(data[0], data[1], data[2], data[3]);
            imuCuboidFrame->setOrientation(quaternion);
            //imuCuboidFrame->setOrientation(quaternion.recipricol());
        }
    }

    void GUIFrame::quaternionFrequency(unsigned int frequency)
    {
        wxString text = "Quaternion: ";
        text << frequency << " Hz";
        quaternionText->GetEventHandler()->CallAfter(
                &wxStaticText::SetLabel, text);
    }

    void GUIFrame::analogueData(const ma::Vector& data)
    {
        ++analoguePackets;
        analoguePanel->addData(data);
    }

    void GUIFrame::analogueFrequency(unsigned int frequency)
    {
        wxString text = "Analogue: ";
        text << frequency << " Hz";
        analogueText->GetEventHandler()->CallAfter(
                &wxStaticText::SetLabel, text);
    }

    void GUIFrame::registerDataReceived(const RegisterData& data)
    {
        registerData = data;
        waitingForRegisterData = false;
    }

    void GUIFrame::onIdle(wxIdleEvent& evt)
    {
        std::chrono::nanoseconds now =
                std::chrono::duration_cast<std::chrono::nanoseconds>(
                std::chrono::steady_clock::now().time_since_epoch());
        unsigned long ns = (now - lastUpdate).count();
        if (ns < 1e9) return;
        double frequency = 1e9 / ns;
        accelerometerFrequency(frequency * accelerometerPackets);
        gyroscopeFrequency(frequency * gyroscopePackets);
        magnetometerFrequency(frequency * magnetometerPackets);
        quaternionFrequency(frequency * quaternionPackets);
        analogueFrequency(frequency * analoguePackets);
        accelerometerPackets = 0;
        gyroscopePackets = 0;
        magnetometerPackets = 0;
        quaternionPackets = 0;
        analoguePackets = 0;
        lastUpdate = now;
    }

    unsigned int GUIFrame::getSampleFrequency()
    {
        // PR = Fcy / (tf * PS)
        // tf = Fcy / (PR * PS)
        int Fcy = FOSC / 2;
        unsigned short PR = registerData.getPeriod();
        short PS = registerData.getPreScalerValue();
        return Fcy / (PR * PS);
    }

    bool GUIFrame::getRegisterData()
    {
        waitingForRegisterData = true;
        setStatusMessage("Getting register data...");
        api->getRegisters();
        double start = std::clock();
        while (waitingForRegisterData
                && (std::clock() - start)/CLOCKS_PER_SEC < 1);
        if (waitingForRegisterData)
            setStatusMessage("Failed to get register data!");
        else
            setStatusMessage("Register data retrieved.");
        return !waitingForRegisterData;
    }

    void GUIFrame::getSensorData(ma::Str filename,
            ma::Vector sensorData[SENSORS][AXES],
            unsigned int sensorLines[SENSORS])
    {
        dataFile.open(filename.c_str(), std::ios_base::in);
        while (dataFile.good())
        {
            ma::Str thisSensorName = ma::getNext(dataFile, ',');
            for (unsigned int sensor = 0; sensor < SENSORS; ++sensor)
                if (thisSensorName == sensorName[sensor]) ++sensorLines[sensor];
            // Clear line.
            ma::getNext(dataFile);
        }
        for (unsigned int sensor = 0; sensor < SENSORS; ++sensor)
            for (unsigned int axis = 0; axis < AXES; ++axis)
                sensorData[sensor][axis] = ma::Vector(sensorLines[sensor]);
        dataFile.clear();
        dataFile.seekg(std::ios::beg);
        // Clear header line.
        ma::getNext(dataFile);
        unsigned int sensorLine[SENSORS] = {0};
        while (dataFile.good())
        {
            ma::Str thisSensorName = ma::getNext(dataFile, ',');
            short x, y, z;
            char comma;
            dataFile >> x; dataFile >> comma;
            dataFile >> y; dataFile >> comma;
            dataFile >> z;
            if (dataFile.good())
            {
                for (unsigned int sensor = 0; sensor < SENSORS; ++sensor)
                    if (thisSensorName == sensorName[sensor])
                    {
                        sensorData[sensor][0][sensorLine[sensor]] = x;
                        sensorData[sensor][1][sensorLine[sensor]] = y;
                        sensorData[sensor][2][sensorLine[sensor]] = z;
                        ++sensorLine[sensor];
                    }
            }
            // Clear line.
            ma::getNext(dataFile);
        }
        dataFile.close();
    }

    void GUIFrame::filterData(ma::Vector sensorData[SENSORS][AXES],
            unsigned int cutoff)
    {
//        unsigned int sampleFrequency = getSampleFrequency();
//        for (unsigned int sensor = 0; sensor < SENSORS; ++sensor)
//            for (unsigned int axis = 0; axis < AXES; ++axis)
//                ma::lowPassFilter(sensorData[sensor][axis], cutoff,
//                        sampleFrequency);
    }

    void GUIFrame::plotData(const ma::Vector sensorData[SENSORS][AXES],
            const unsigned int sensorLines[SENSORS])
    {
        for (unsigned int sensor = 0; sensor < SENSORS; ++sensor)
        {
            ma::Str frameName = sensorName[sensor] + " Calibration Data";
            wxFrame* plotFrame = new wxFrame(
                    this, wxID_ANY, frameName.c_str());
            ma::Plot2DPanel* plotPanel = new ma::Plot2DPanel(plotFrame, 3, 0,
                    sensorLines[sensor], -32768, 32768);
            for (unsigned int axis = 0; axis < AXES; ++axis)
                plotPanel->setYData(sensorData[sensor][axis], axis);
            plotFrame->Show();
        }
    }

    void GUIFrame::setAPIType(wxCommandEvent& event)
    {
        wxSingleChoiceDialog selectAPITypeDialog (this,
                wxT("nuIMU Data Interface Types:"),
                wxT("Select nuIMU Data Interface Type"), imuTypes, imuTypeNames);
        selectAPITypeDialog.SetSelection(currentAPIType);
        if (selectAPITypeDialog.ShowModal() == wxID_CANCEL) return;
        setAPIType(selectAPITypeDialog.GetSelection());
    }

    void GUIFrame::setAPIType(unsigned int typeIndex)
    {
        ma::Str portname;
        if (api == 0)
            portname = portstring;
        else
        {
            portname = api->getPort();
            delete api;
        }
        switch (typeIndex)
        {
        case 0:
            api = new API();
            break;
        case 1:
            api = new FileDataAPI(0);
            break;
        case 2:
            api = new FileRawAPI(0);
            break;
        }
        currentAPIType = typeIndex;
        api->addListener(listener);
        connect(portname);
    }

    void GUIFrame::connect(wxCommandEvent& event)
    {
        wxTextEntryDialog connectDialog (this, wxT("Port:"),
                wxT("Connect to Port"));
        if (connectDialog.ShowModal() == wxID_CANCEL) return;
        ma::Str port(connectDialog.GetValue().c_str());
        setStatusMessage(ma::Str("Connecting to port: " + port));
        connect(port);
    }

    bool GUIFrame::connect(const ma::Str& port)
    {
        if (api->setPort(port.c_str()))
        {
            ma::Str message("Connected to: ");
            message += api->getPort();
            setStatusMessage(message);
            return true;
        }
        else
        {
            ma::Str error("Failed to connect to ");
            error += port + "!";
            wxMessageDialog message(this, error.c_str());
            message.ShowModal();
            return false;
        }
    }

    void GUIFrame::testDevice(wxCommandEvent& event)
    {
        api->testDevice();
        setStatusMessage("Sent test device command.");
    }

    void GUIFrame::sendIMUData(wxCommandEvent& event)
    {
        api->startSendingIMUData();
        setStatusMessage("Sent start sending IMU data command.");
    }

    void GUIFrame::sendQuaternions(wxCommandEvent& event)
    {
        api->startSendingQuaternions();
        setStatusMessage("Sent start sending quaternions command.");
    }

    void GUIFrame::stopSending(wxCommandEvent& event)
    {
        api->stopSending();
        setStatusMessage("Sent stop sending command.");
    }

    void GUIFrame::calibrate(wxCommandEvent& event)
    {
        if (!getRegisterData())
        {
            wxMessageDialog errorMessage (this,
                    wxT("Unable to retreieve register data from IMU\n"
                        "Ensure the IMU is connected"),
                    wxT("Unalbe to connect"));
            errorMessage.ShowModal();
            return;
        }
        wxMessageDialog startMessage (this,
                wxT("Place the IMU on the turntable away from any magnetic "
                    "interference, or large lump of ferrous metals.\n"
                    "Slowly begin to revolve the turntable. The slower it is "
                    "revolved, the more accurate the values will be.\n"
                    "Once a full rotation has been completed, turn the IMU "
                    "over, preferrably only rotating around one axis.\n"
                    "Revolve the turntable around one full rotation again.\n"
                    "Note: It is better to rotate a few degrees more than 360 "
                    "than to rotate less than 360. Going past the 360 point "
                    "will not affect the result.\n\n"
                    "Press OK to begin."),
                wxT("Begin Calibration"));
        startMessage.ShowModal();
        setStatusMessage("Calibrating...");
        api->stopSending();
        ma::Str filename = "CalibrationData.csv";
        dataFile.open(filename.c_str(),
                std::ios_base::out | std::ios_base::trunc);
        dataFile << "Sensor,X,Y,Z" << std::endl;
        saveData = true;
        api->startSendingIMUData();
        wxMessageDialog finishMessage (this,
                wxT("Revolve the turntable around one full rotation.\n"
                    "Flip the IMU.\n"
                    "Revolve the turntable around one full rotation again.\n\n"
                    "Press OK when complete."),
                wxT("End Calibration"));
        finishMessage.ShowModal();
        api->stopSending();
        saveData = false;
        dataFile.close();
        unsigned int sensorLines[SENSORS] = {0};
        ma::Vector sensorData[SENSORS][AXES];
        getSensorData(filename, sensorData, sensorLines);
        plotData(sensorData, sensorLines);
        // Low pass filter data with a 1Hz cutoff.
        filterData(sensorData, 10);
        plotData(sensorData, sensorLines);
//        unsigned int deletedLines = 0;
//        for (unsigned int line = 0;
//               line < sensorLines[0]
//            && line < sensorLines[1]
//            && line < sensorLines[2]; ++line)
//        {
//            for (unsigned int axis = 0; axis < AXES; ++axis)
//            {
//                if (sensorData[0][axis][line] > 1000)
//                {
//                    std::cout << line << " - Gyro: " << sensorData[0][0][line] << ", " << sensorData[0][1][line] << ", " << sensorData[0][2][line] << std::endl;
//                    for (unsigned int i = 0; i < AXES; ++i)
//                    {
//                        sensorData[1][i][line] = 0;
//                        sensorData[2][i][line] = 0;
//                    }
//                    ++deletedLines;
//                    break;
//                }
//            }
//        }
//        std::cout << "Deleted lines: " << deletedLines << " of " << sensorLines[0] << std::endl << std::endl;
        setStatusMessage("Finished calibrating.");
    }

    void GUIFrame::command(wxCommandEvent& event)
    {
        bool valid = false;
        ma::Str command;
        while (!valid)
        {
            wxTextEntryDialog commandDialog (this, wxT("Command:"),
                    wxT("Enter Command"));
            if (commandDialog.ShowModal() == wxID_CANCEL) return;
            command = ma::Str(commandDialog.GetValue().c_str());
            valid = command.length() == 4;
            if (!valid)
            {
                wxMessageDialog error (this,
                        wxT("Must be four characters long."),
                        wxT("Invalid command!"));
                error.ShowModal();
            }
        }
        api->sendCommand(ma::Str(command));
        setStatusMessage("Command " + command + " sent.");
    }

    void GUIFrame::setRegister(wxCommandEvent& event)
    {
        bool numberValid = false;
        unsigned long registerNumber;
        long longValue;
        short shortValue;
        while (!numberValid)
        {
            bool valueValid = false;
            wxTextEntryDialog registerNumberDialog (this,
                    wxT("Register number:"),
                    wxT("Enter Number of Register to Set"));
            if (registerNumberDialog.ShowModal() == wxID_CANCEL) return;
            numberValid = registerNumberDialog.GetValue().
                    ToULong(&registerNumber);
            numberValid &= registerNumber < 44 && registerNumber % 2 == 1;
            if (!numberValid)
            {
                wxMessageDialog error (this,
                        wxT("Must be an odd number < 44"),
                        wxT("Invalid Number!"));
                error.ShowModal();
            }
            while (numberValid && !valueValid)
            {
                wxTextEntryDialog registerValueDialog (this,
                        wxT("Register value:"),
                        wxT("Enter New Register Value"));
                if (registerValueDialog.ShowModal() == wxID_CANCEL) return;
                valueValid = registerValueDialog.GetValue().ToLong(&longValue);
                if (valueValid)
                {
                    shortValue = (short)longValue;
                    valueValid = shortValue == longValue;
                }
                if (!valueValid)
                {
                    valueValid = false;
                    wxMessageDialog error (this,
                            wxT("Must be a number between -32768 and 32767"),
                            wxT("Invalid Number!"));
                    error.ShowModal();
                }
            }
        }
        if (api->setRegister(registerNumber, shortValue))
        {
            ma::Str message("Sent set register " + ma::toStr(registerNumber));
            message += " to " + ma::toStr(shortValue);
            setStatusMessage(message);
        }
        else
        {
            setStatusMessage("Failed to send register set command.");
        }
    }

    void GUIFrame::configureRegisters(wxCommandEvent& event)
    {
        if (!getRegisterData())
        {
            wxMessageDialog message(this,
                    wxT("Failed to retrieve register data."));
            message.ShowModal();
        }
        else
        {
            setStatusMessage("Configuring register data.");
            Config configDialog(this, &registerData);
            int returnCode = configDialog.ShowModal();
            if (returnCode == wxID_CANCEL)
            {
                setStatusMessage("Register update cancelled.");
                return;
            }
            if (!api->setRegisters(&registerData))
            {
                setStatusMessage("Register update failed!");
                return;
            }
            setStatusMessage("Register update sent.");
            if (returnCode == wxID_APPLY)
            {
                if (!api->saveRegistersToFlash())
                {
                    setStatusMessage("Save to flash failed.");
                    return;
                }
                setStatusMessage("Register update and save to flash sent.");
            }
        }
    }

    void GUIFrame::saveRegistersToFlash(wxCommandEvent& event)
    {
        api->saveRegistersToFlash();
    }

    void GUIFrame::freeze(wxCommandEvent& event)
    {
        accelerometerPanel->freezeOutput(event.IsChecked());
        gyroscopePanel->freezeOutput(event.IsChecked());
        magnetometerPanel->freezeOutput(event.IsChecked());
        quaternionPanel->freezeOutput(event.IsChecked());
        analoguePanel->freezeOutput(event.IsChecked());
    }

    void GUIFrame::display(wxCommandEvent& event)
    {
        switch (event.GetId())
        {
        case ID_DISPLAY_CUBE:
            if (imuCuboidFrame == 0)
            {
                imuCuboidFrame = new ma::CuboidFrame(this, ID_DISPLAY_CUBE,
                        wxT("Device Orientaion"), wxPoint(0, 0),
                        wxSize(500, 500));
                imuCuboidFrame->Bind(wxEVT_CLOSE_WINDOW,
                        &GUIFrame::childClosed, this);
                imuCuboidFrame->SetIcon(wxIcon(wxT("imu.ico")));
                imuCuboidFrame->Show();
            }
            else
            {
                imuCuboidFrame->Raise();
            }
            break;
        case ID_DISPLAY_ACCELEROMETER:
            if (accelerometerVectorFrame == 0)
            {
                accelerometerVectorFrame = new ma::Vector3DFrame(
                        (long)api->getAccelerometerScale(), this,
                        ID_DISPLAY_ACCELEROMETER, wxT("Accelerometer Vector"),
                        wxPoint(500, 0), wxSize(500, 500));
                accelerometerVectorFrame->Bind(wxEVT_CLOSE_WINDOW,
                        &GUIFrame::childClosed, this);
                accelerometerVectorFrame->SetIcon(wxIcon(wxT("imu.ico")));
                accelerometerVectorFrame->Show();
            }
            else
            {
                accelerometerVectorFrame->Raise();
            }
            break;
        case ID_DISPLAY_GYROSCOPE:
            if (gyroscopeVectorFrame == 0)
            {
                gyroscopeVectorFrame = new ma::Vector3DFrame(
                        (long)api->getGyroscopeScale(), this,
                        ID_DISPLAY_GYROSCOPE, wxT("Gyroscope Vector"),
                        wxPoint(500, 0), wxSize(500, 500));
                gyroscopeVectorFrame->Bind(wxEVT_CLOSE_WINDOW,
                        &GUIFrame::childClosed, this);
                gyroscopeVectorFrame->SetIcon(wxIcon(wxT("imu.ico")));
                gyroscopeVectorFrame->Show();
            }
            else
            {
                gyroscopeVectorFrame->Raise();
            }
            break;
        case ID_DISPLAY_MAGNETOMETER:
            if (magnetometerVectorFrame == 0)
            {
                magnetometerVectorFrame = new ma::Vector3DFrame(
                        (long)api->getMagnetometerScale(), this,
                        ID_DISPLAY_MAGNETOMETER, wxT("Magnetometer Vector"),
                        wxPoint(1000, 0), wxSize(500, 500));
                magnetometerVectorFrame->Bind(wxEVT_CLOSE_WINDOW,
                        &GUIFrame::childClosed, this);
                magnetometerVectorFrame->SetIcon(wxIcon(wxT("imu.ico")));
                magnetometerVectorFrame->Show();
            }
            else
            {
                magnetometerVectorFrame->Raise();
            }
            break;
        }
    }

    void GUIFrame::analogueBitsChanged(wxCommandEvent& event)
    {
        setAnalogueBits(event.GetInt());
    }

    void GUIFrame::setAnalogueBits(unsigned int analogueBits)
    {
        api->setAnalogueBits(analogueBits);
        wxString scaleString = wxT("Bits: (");
        scaleString << analogueBits << ")";
        analogueBitsText->GetEventHandler()->CallAfter(
                &wxStaticText::SetLabel, scaleString);
    }

    void GUIFrame::childClosed(wxCloseEvent& event)
    {
        switch (event.GetId())
        {
        case ID_DISPLAY_CUBE:
            imuCuboidFrame->Destroy();
            imuCuboidFrame = 0;
            break;
        case ID_DISPLAY_ACCELEROMETER:
            accelerometerVectorFrame->Destroy();
            accelerometerVectorFrame = 0;
            break;
        case ID_DISPLAY_GYROSCOPE:
            gyroscopeVectorFrame->Destroy();
            gyroscopeVectorFrame = 0;
            break;
        case ID_DISPLAY_MAGNETOMETER:
            magnetometerVectorFrame->Destroy();
            magnetometerVectorFrame = 0;
            break;
        }
    }

    const ma::Str GUIFrame::sensorName[SENSORS] =
    {
        "Gyroscope",
        "Accelerometer",
        "Magnetometer"
    };

    const ma::Str GUIFrame::axisName[AXES] =
    {
        "X",
        "Y",
        "Z"
    };

    const unsigned int GUIFrame::imuTypes = 3;
    const wxString GUIFrame::imuTypeNames[] =
    {
        wxT("nu"),
        wxT("nuDataFile"),
        wxT("nuRawFile")
    };
}
