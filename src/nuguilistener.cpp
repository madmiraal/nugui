// (c) 2016 - 2018: Marcel Admiraal

#include "nuguilistener.h"

#include "nuguiframe.h"

#include "vector.h"
#include "imudata.h"

#include <chrono>
#include <iostream>

namespace nu
{
    GUIListener::GUIListener(GUIFrame* frame) : frame(frame)
    {
    }

    void GUIListener::onMessageDataReceived(const ma::Data& data)
    {
        std::cout << "Message received: " << data.getStringData() << std::endl;
        frame->setStatusMessage(data.getStringData());
    }

    void GUIListener::onAccelerometerDataReceived(const ma::Data& data)
    {
        ma::Vector vectorData = data.getVectorData();
        std::cout << "Accelerometer data received: ";
        vectorData.print();
        std::cout << std::endl;
        frame->accelerometerData(vectorData);
    }

    void GUIListener::onGyroscopeDataReceived(const ma::Data& data)
    {
        ma::Vector vectorData = data.getVectorData();
        std::cout << "Gyroscope data received: ";
        vectorData.print();
        std::cout << std::endl;
        frame->gyroscopeData(vectorData);
    }

    void GUIListener::onMagnetometerDataReceived(const ma::Data& data)
    {
        ma::Vector vectorData = data.getVectorData();
        std::cout << "Magnetometer data received: ";
        vectorData.print();
        std::cout << std::endl;
        frame->magnetometerData(vectorData);
    }

    void GUIListener::onQuaternionReceived(const ma::Data& data)
    {
        ma::Vector vectorData = data.getVectorData();
        std::cout << "Quaternion data received: ";
        vectorData.print();
        std::cout << std::endl;
        frame->quaternionData(vectorData);
    }

    void GUIListener::onAnalogueDataReceived(const ma::Data& data)
    {
        ma::Vector vectorData = data.getVectorData();
        std::cout << "Analogue data received: ";
        data.getVectorData().print();
        std::cout << std::endl;
        frame->analogueData(vectorData);
    }

    void GUIListener::onRegisterDataReceived(const ma::Data& data)
    {
        RegisterData registerData(data.getVectorData());
        frame->registerDataReceived(registerData);
    }
}
