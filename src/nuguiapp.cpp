// (c) 2016 - 2017: Marcel Admiraal

#include "nuguiapp.h"

#include "nuguiframe.h"

IMPLEMENT_APP(nu::GUIApp);

namespace nu
{
    bool GUIApp::OnInit()
    {
        GUIFrame* frame = new GUIFrame(0L);
        frame->Show();
        return true;
    }
}
