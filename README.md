# nuGUI #

Cross-platform C++ code for a graphical user interface to manage the nu IMU.

## Dependencies ##
This code depends on:

* [wxWidgets](https://www.wxwidgets.org/)
* [MAUtils](https://bitbucket.org/madmiraal/mautils)
* [MAwxUtils](https://bitbucket.org/madmiraal/mawxutils)
* [MAIMU](https://bitbucket.org/madmiraal/maimu)
